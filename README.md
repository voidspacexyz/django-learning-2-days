## Django Workshop (2 Days) 

This syllabus is written with Free software movements in mind. Assuming a non-residential camp, we would have about 7 hours a day. 9AM - 1PM ( 4 hours ) and 2PM - 5PM ( 3 hours ) 

- No camp ever start @ 9 so leaving 45 min before
- Breaks are never 10 min, so actually assuming (20 min * 2) for the same - So calculating, we would have roughly 5:30 hours a day. 
- The tech sessions are planned for only 4 hours a day, keeping 45 min of buffer depending on the audience, and leaving the rest of the time for the FOSS organization to engage with the audience for about an hour everyday.

##  Day 1 : I am assuming the following topics are covered along with python 

- Data structures -> Variables, data types, lists, dict, tuples (nested also) 	
- For loops (no need of while etc) 	- Conditions , if, elif, else 	
- Functions and returns 	
- Classes and Objects along with a simple inheritance 	
- importing modules 
- importing other files 	
- importing python project folders(the need for __init__.py files) 	
- (Optional) Function Decorators 	
- Day 2 requirements are instructed about

## Day 2 : Tools Needed : ( Checking 10 min ) 

- Django Installed (with / without virtualenv) - Mozilla Firefox Browser 	
- Rested Firefox Add-on Installed in the browser 

## Step -1 : A note about vekkam, manam, sudu, sorana and its impact on learning ( 2 min ) 

## Step 0 : Understanding HTTP , Request and Response, Browsers ( 20 min ) 	

- Http Methods (GET, POST, DELETE, PUT) 	- What is Request and Response. 	
- What is the use of a browser, how to use a browser and how HTTP applies in Browser

## Step 1 : Developing MVC and the Need. What it means. ( 10 min ) 	 

## Break ( 10 min ) 

## Step 2 : Creating a project and Hello World from Django ( 30 min ) 	

- Installing Django 	
- Django-admin and how to start a project 	
- Looking at the django landing page 
- Understanding the project structure (urls, settings and manage.py alone) 	
- How to run the project 

## Step 3 : Writing a custom Hello World ( 30 min ) 	

- Editing the URL to include a / url 	
- Django JsonResponse in Views 

## Lunch ( 45 min ) 

## Step 4 : More of JsonResponse ( 20 min ) 	

- JsonResponse to return list, dict, tuples , nested responses, 	
- status code and HTTP Status 

## Step 5 : Database and models  ( 40 min )

 - Understanding SQL , tables, and rows and columns 	
- Class and objects revision 	
- Writing ORM with classes (todo name, description, date and username) 	
- Django shell
- using django shell to create list and delete rows 

## Break ( 10 min ) 

## Step 6 : Getting models to work in views( listing alone ) ( 30 min ) 	

- Importing files(models.py) 
- objects revision to load data from models 	- Using Rested  	
- Returning queuries with json response 

## End of Day 1 

## Day 2 

## Step 7 : CRUD using views and Rested ( 60 min ) 	

- Using Rested to do POST, GET, DELETE 	- Introduction to decorators 	
- Django HttpMethods Decorator 

## Break ( 10 min ) 

## Step 8 : Django's User model ( 50 min ) 	

- Django table to include django user model 
- django migrations 
- Django createsuperuser 	and django admin
- Getting user in views 	
- applying a user into database 

## Lunch 

## Step 9 : Basic Authentication ( 45 min ) 	

- Using user model to do authentication 	- Login / Logout views 
- Django settings to include Login Redirect url 

## Break ( 10 min ) 

## Step 10 : API authentication ( 30 min ) 	

- Create another table called apikeys 	
- How to create api keys 	
- Authentication using api keys 	
- User identification using api keys 

## Step 11 : Showcasing some large projects in django ( 30 min ) 

- Django cookiecutter 	
- Django Rest Framework 	
- How to go forward and what projects you can do for the hackathon 	
- Some FOSS inclined industry person's guest session 

### Hugs, smokes, break-ups,  and the tuf part of team forming to start